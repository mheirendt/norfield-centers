import {
    Body,
    Controller,
    Get,
    Query,
    Path,
    Post,
    Route,
    SuccessResponse,
} from "tsoa";
import Center from '@norfield/center';
import Julie from '../../seed/centers/julie.json';
import Usan from '../../seed/centers/usan.json';
import { ICenter } from "@norfield/common";
// import { Field, Priority, TicketTag, ICenter } from "@norfield/common";

const centers = [Julie, Usan];


@Route("centers")
export class CenterController extends Controller {

    /**
     * Get all the centers
     */
    @Get()
    public async getAll() {
        return [];
    }

    /**
     * Get an 811 call center by it's name
     * @param name The name of the center
     */
    @Get("{name}")
    public async getCenter(
        @Path() name: string,
    ): Promise<ICenter> {
        const center = centers.find(c => c.name.toLowerCase() === name.toLowerCase());
        return new Center(center);
    }

    @SuccessResponse("201", "Created")
    @Post()
    public async createCenter(
        @Body() requestBody: ICenter
    ): Promise<void> {
        this.setStatus(201);
        return;
    }
}